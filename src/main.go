package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	_ "github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

var db *sql.DB
var err error

type userData struct {
	Id string `json:"userId"`
	Name string `json:"name"`
	Password string `json:"password"`
	Email string `json:"email"`
}

type productData struct {
	ProductID string `json:"productID"`
	ProductName string `json:"name"`
	Price string `json:"price"`
	Description string `json:"Desc"`
	ProductType string `json:"type"`
	Carbon_Footprint string `json:"Carbon_Footprint"`
	Image_src string `json:"image_src"`


}

type productDataArray struct {
	Products []productData `json:"products"`
}

type carbonEmission struct {
	CarbonEmissionVal int64 `json:"c_emission"`
}

func main() {
	//databaseConnection();

	handleRequests();


}

func HomePage(w http.ResponseWriter, r *http.Request){

	fmt.Println("Home page hit")

}

// method for login
func handleLogin(w http.ResponseWriter, r *http.Request){

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	keyVal := make(map[string]string)
	json.Unmarshal(body, &keyVal)

	databaseConnection();
	var userdata  userData

	// select query to get data
	err = db.QueryRow("SELECT userID, username, email FROM user WHERE email = ? AND password = ?", keyVal["email"], keyVal["password"]).Scan(&userdata.Id, &userdata.Name, &userdata.Email)// ? = placeholder
	if err != nil {
		json.NewEncoder(w).Encode("Invalid")
		return
		//panic(err.Error()) // proper error handling instead of panic in your app
	}

	defer db.Close()



	json.NewEncoder(w).Encode(&userdata)




}

func handleRegister(w http.ResponseWriter, r *http.Request){

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}

	keyVal := make(map[string]string)
	json.Unmarshal(body, &keyVal)

	databaseConnection();

	//fmt.Print("INSERT INTO user VALUES( keyVal['email'], keyVal['name'] , keyVal['email']  )")
		// Register Query
	//Prepare statement for inserting data
	stmtIns, err := db.Prepare("INSERT INTO user(email, username, password) VALUES( ?, ? , ? )") // ? = placeholder
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	//_, err = stmtIns.Exec(title)
	defer stmtIns.Close() // Close the statement when we leave main() / the program terminates



	email := keyVal["email"]
	name := keyVal["name"]
	password := keyVal["password"]

	_, err = stmtIns.Exec(email, name, password)
	if err != nil {
		panic(err.Error())
	}

	defer db.Close()

	fmt.Fprintf(w, "New User created")
	//log.Println(keyVal["email"])
	//log.Println("jsjsjs"+t.name)
}

// method to get all products
func handleAllProducts(w http.ResponseWriter, r *http.Request){
	databaseConnection();
	var productsdata  []productData

	result, err := db.Query("SELECT * FROM product") // ? = placeholder
	if err != nil {
		json.NewEncoder(w).Encode("No Products Found")
		return
		//panic(err.Error()) // proper error handling instead of panic in your app
	}

	defer result.Close()
	for result.Next() {
		var singleproductdata productData
		err := result.Scan(&singleproductdata.ProductID, &singleproductdata.ProductName, &singleproductdata.Price, &singleproductdata.Description , &singleproductdata.ProductType, &singleproductdata.Carbon_Footprint, &singleproductdata.Image_src )
		if err != nil {
			panic(err.Error())
		}
		productsdata = append(productsdata, singleproductdata)
	}

	defer db.Close()

	// need my value as "products" key
	products := productDataArray{
		Products: productsdata,
	}

	json.NewEncoder(w).Encode(products)
}


func handleCarbonEmissionData(w http.ResponseWriter, r *http.Request){

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}

	keyVal := make(map[string]string)
	json.Unmarshal(body, &keyVal)

	//fmt.Println(keyVal)

	//f := "10"
	 carEmissionFloatVal, err := strconv.ParseInt(keyVal["Car"],10,64);
	//carEmissionFloatVal, err := strconv.ParseFloat(keyVal["Car"], 64)
	busEmissionFloatVal , err := strconv.ParseInt(keyVal["Bus"],10, 64)
	TaxiEmissionFloatVal , err := strconv.ParseInt(keyVal["Taxi"], 10,64)
	TrainEmissionFloatVal , err := strconv.ParseInt(keyVal["Train"], 10,  64)
	TramEmissionFloatVal , err := strconv.ParseInt(keyVal["Tram"], 10, 64)


	// 0.0001 meteric ton/km  ->  annually
	carEmission := int64(1)
	busEmission := int64(6)
	TaxiEmission := int64(4)
	TramEmission := int64(3)
	TrainEmission := int64(10)

	totalEmission := carEmission *  carEmissionFloatVal + busEmission * busEmissionFloatVal + TaxiEmission * TaxiEmissionFloatVal + TrainEmission * TrainEmissionFloatVal + TramEmission * TramEmissionFloatVal

	//fmt.Println(totalEmission)
	//totalEmissionStringVal := fmt.Sprintf("%f", totalEmission)
	//stringVal := fmt.Sprintf("%f", carEmissionFloatVal)
	//
	//fmt.Println("vvv"+stringVal)

	carbonData := carbonEmission{
		CarbonEmissionVal:  totalEmission,
	}

	json.NewEncoder(w).Encode(carbonData)


}
func handleRequests(){
	router := mux.NewRouter()
	// Index Page API
	router.HandleFunc("/", HomePage).Methods("POST")
	// Login  API
	router.HandleFunc("/login", handleLogin).Methods("POST")
	// Register  API
	router.HandleFunc("/register", handleRegister).Methods("POST")

	// All products api
	router.HandleFunc("/products", handleAllProducts).Methods("GET")

	// find carbon emission api
	router.HandleFunc("/carbon", handleCarbonEmissionData).Methods("POST")


	log.Fatal(http.ListenAndServe(":8081", router))
}


func databaseConnection(){

	db, err = sql.Open("mysql", "root:admin@123@/VoonexHackathone")
	if err != nil {
		panic(err.Error())
	}

	//defer db.Close()

	// Open doesn't open a connection. Validate DSN data:
	err = db.Ping()
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	// Use the DB normally, execute the querys etc

	fmt.Println("Database running")
}